# ODRL Profile for Attribute based access/usage control using Verifiable Credential claims

## Abstract
This document is defining a public ODRL Profile to be used in any context.
However, the context of use in which this Profile is intended, is in a data exchange between two or more parties.

## Parties
In this document we will be referring to 2 parties:
- **The provider**: The party owning all relevant rights over the data or service, this is the party defining the policy. This policy can be referenced with the [Offer](https://www.w3.org/TR/odrl-vocab/#term-Offer) class.
- **The consumer**: The party wanting to use the data or service, this party must define a usage policy also referenced by a [Set](https://www.w3.org/TR/odrl-vocab/#term-Set) class.

## Introduction

### Background
The Open Digital Rights Language [ODRL](https://www.w3.org/TR/odrl-model/) is a policy expression language that provides a flexible and interoperable information model for representing statements about the usage of content and services. However, there is no easy way to verify and assess an access request in a trustworthy verifiable manner from that same policy, especially in a machine-readable way.
On another note, [W3C verifiable credentials](https://www.w3.org/TR/vc-data-model-2.0/) are a digital representation of information that can be cryptographically verified. They are used to establish trust and authenticate individuals or entities in online transactions and interactions.
Therefore, this Profile attempts to bring together both to define policies which can use Verifiable Credentials.

### Purpose of the ODRL Profile
The main intention of this ODRL Profile is to be able to refer in a clear and precise way to verifiable credential claims in an ODRL Policy. This would give assignors of policies a way to enforce policies using trustworthy and verifiable claims from an assignee, and in doing so having more trust and confidence in the enforcement of the policy.

### Scope and Applicability
There are no limitations or a predefined scope for this profile. But it’s intended especially for ecosystems already using verifiable credentials and looking to define policies on targets using those verifiable credentials.

## Use cases
- A provider restricting data to a geographical location
- A provider restricting data to a specified license holders
- A policy requiring a membership credential
- A policy for a specific set of authorized legal entities
- Or any case in which using a verifiable credential is relevant to the policy

## Profile Defined Constraint

| Name                      | Label                 | Identifier                                          | Parent class    | 
|---------------------------|-----------------------|-----------------------------------------------------|-----------------|
| ovc:Constraint            | Constraint            | https://w3id.org/gaia-x/ovc/1/constraint            | odrl:Constraint |
| ovc:leftOperand           | LeftOperand           | https://w3id.org/gaia-x/ovc/1/leftOperand           |                 |
| ovc:credentialSubjectType | CredentialSubjectType | https://w3id.org/gaia-x/ovc/1/credentialSubjectType |                 |                              


### ovc:constraint
A constraint is a way to refine an odrl:rule. An ovc:constraint concerns a refinement using Verifiable Credentials, in which the assignee is the holder, consequently a constraint can be defined in an odrl:rule. It MUST contain an ovc:leftOperand, an ovc:credentialSubjectType, an odrl:operator and an odrl:rightOperand.
**Example:** A constraint on the location of the assignee where a constraint contains: ovc:leftOperand the attribute to evaluate in JSONPath, ovc:credentialSubjectType with the type of the verifiable credential, an odrl:operator with an odrl:eq and an odrl:rightOperand with the specified locations.

### ovc:leftOperand
A LeftOperand is a way to refer to a W3C Verifiable Credential attribute to evaluate against an odrl:rightOperand. An ovc:leftOperand MUST contain an attribute of a W3C Verifiable Credential to be evaluated and MUST be in the format of a JSONPath.
**Example:** A LeftOperand might refer to an occupation of an individual in Identification Verifiable Credential '$.profession.label'

### ovc:credentialSubjectType
A CredentialSubjectType is a way to clearly define the type of which W3C Verifiable Credential the ovc:constraint is intended for. An ovc:credentialSubjectType MUST contain the type of the intended W3C Verifiable Credential to evaluate, a relevant context MUST be included for namespacing concerns.
**Example:** A CredentialSubjectType might refer to kind of personal identification such as 'gov:PersonalIdentification'

## Sample ODRL Policies
A sample policy in [json-ld] requiring the assignee to:
- be a holder of verifiable credentials of type `LegalParticiapant` defined in the Gaia-X ontology
- And for that credential to have the attribute 
````jsonpath
$.credentialSubject.gx:legalAddress.gx:countrySubdivisionCode
```` 
with a value in these values: `["FR-HDF","BE-BRU"]`

````json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    { "gx" :"https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#" },
    { "ovc": "https://w3id.org/gaia-x/ovc/1/" }
  ],
  "@type": "Offer",
  "uid": "http://example.com/policy/123",
  "profile": "https://w3id.org/gaia-x/ovc/1/",
  "permission": [
    {
      "@type": "Permission",
      "target": "http://example.com/asset/456",
      "action": "http://www.w3.org/ns/odrl/2/play",
      "assigner": "http://example.com/provider",
      "ovc:constraint": [
        {
          "ovc:leftOperand": "$.credentialSubject.gx:legalAddress.gx:countrySubdivisionCode",
          "operator": "http://www.w3.org/ns/odrl/2/isAnyOf",
          "rightOperand": [
            "FR-HDF",
            "BE-BRU"
          ],
          "ovc:credentialSubjectType": "gx:LegalParticipant"
        }
    ]
    }
  ]
}
````
- More samples may be found in the [examples](https://gitlab.com/gaia-x/lab/policy-reasoning/odrl-vc-profile/-/tree/main/examples) folder.

## Reference Implementation

This section is not a requirement to adhere to this sequence or technical implementation but merely proposes a more technical perspective on how the Policy Reasoning engine mentioned below is done.
It might also ease understanding of the aforementioned concepts by accessing the provided sandbox, which also already includes examples for testing this profile.

### Policy reasoning engine 

```mermaid
flowchart LR
    subgraph Knowledge Base
        DB[(Graph Database)]
        ODRL(ODRL Ontology) --> DB
        OVC(OVC Profile) --> DB
    end

    subgraph Inputs
        ProviderPolicy[Provider Offer Policy]
        ConsumerPolicy[Consumer Set Policy]
        ConsumerCredentials[Consumer credentials]
    end

    ProviderPolicy -->|RDF| DB
    ConsumerPolicy --> Query{Reasoning Query}
    ConsumerCredentials -->|jsonPath| Query
    Query <-.->|SPARQL| DB
    
    subgraph Outputs
        Agreement[[Agreement Policy]]
        NoAgreement[[No Agreement Reached]]
    end
    Query ==>|results available| Agreement
    Query ==>|no results| NoAgreement
```

### Sandbox environment
A Proof of concept implementation is available in the [Wizard](https://wizard.lab.gaia-x.eu/policyStepper).
Please note that this demo is still a work in progress and that it might not implement all the odrl specifications.


## Informative references
- [w3c-vc](https://www.w3.org/TR/vc-data-model-2.0/)
- [odrl-model](https://www.w3.org/TR/odrl-model/)
- [odrl-vocab](https://www.w3.org/TR/odrl-vocab/)
- [json-path](https://datatracker.ietf.org/wg/jsonpath/about/)
- [json-ld](https://www.w3.org/TR/json-ld11/)
